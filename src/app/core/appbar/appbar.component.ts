import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appbar',
  templateUrl: './appbar.component.html',
  styleUrls: ['./appbar.component.scss'],
})
export class AppbarComponent implements OnInit {
  open: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  handleOpen(): void {
    this.open = !this.open;
  }
}
