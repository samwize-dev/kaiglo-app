import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppbarComponent } from 'src/app/core/appbar/appbar.component';
import { FooterComponent } from 'src/app/core/footer/footer.component';

@NgModule({
  declarations: [AppbarComponent, FooterComponent],
  imports: [CommonModule, RouterModule, FontAwesomeModule],
  exports: [AppbarComponent, FooterComponent],
})
export class CoreModule {}
