import { Component, OnInit } from '@angular/core';
import {
  faFacebookF,
  faTwitter,
  faInstagram,
  faTelegramPlane,
  faFacebookMessenger,
  faYoutube,
  faPinterest,
} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  fb = faFacebookF;
  tw = faTwitter;
  insta = faInstagram;
  tel = faTelegramPlane;
  fbMsg = faFacebookMessenger;
  yt = faYoutube;
  pin = faPinterest;

  year = new Date().getFullYear();

  constructor() {}

  ngOnInit(): void {}
}
